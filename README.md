<!--
  vim: tabstop=4 softtabstop=4 shiftwidth=4 colorcolumn=72 expandtab
  -->

# "Pure Cambridge" Critical Edition KJV + Apocrypha

This is a complete tab-separated-values (TSV) file containing the "Pure"
Cambridge Critical Edition[^1] of the Cambridge family of King James
Version (KJV) Bibles[^2] along with the Apocrypha[^3].  It is complete
with Red letters[^4], italics[^5], brackets[^6], paragraph markers, and
was made as a drop-in replacement for the Oxford version TSV file used
in the `kjv` \*NIX program created by Bontibon[^7].

To date (07 September 2022), it is thought that there are no
misspellings, grammatical mistakes, etc. that are not present in the
PCE.  However, it is suspected that the Apocrypha may have numerous
mistakes, as many were found at random while creating the TSV.  It is
the Apocrypha as translated by the King James translators, but where it
came from on the internet is unknown.  I took it from Luke Smith's
fork[^8] of the `kjv` program and had to add two entire books that were
missing.

As of today (07 September 2022), paragraphication was added to the
apocrypha, taken from the version available from Faith Life for their
LOGOS 9 bible software.  These paragraph markers, as they were not in
the original KJV 1611, have been added as pilcrows inside of a pair of
doubled less-than/greater-than symbols with a space after each
(`<<¶>> `).  Paragraphication has also been added to the New Testament
because it abruptly stopped at the end of Acts as well as to Matthew
and Luke's famous genealogies of Jesus.  These paragraph markers are
the same as those added to the apocrypha, but they are a capital Greek
letter Kappa (for _κεφάλαιο_, meaning _chapter_) followed by a period
(`<<Κ.>> `).  This, too, mostly follows how Faith Life's edition of the
KJV follows, with slight modifications.

Also included is a complete list of the vocabulary used in this TSV in
the form of a Vim spell file.  All of it is alphabetically sorted.  The
English is of better use than the endless unique names in the Old and
New Testaments and the Apocrypha.

## About the TSV

The TSV syntax looks like this:

    Book Title<tab>Book-Abbreviation<tab>Book-Number<tab>Chapter-Number<tab>Verse-Number<tab>Verse
    
    Genesis<tab>Gn<tab>1<tab>1<tab>1<tab>In the beginning God created the heaven and the earth.

The Book-Abbreviations are separated by commas if more than one
available.  You can use `~` in an `awk` script to find any record in a
given column that might contain one, rather than `==`.

The books are arranged (with book numbers in parentheses):

  1. Old Testament (1–39)
  3. New Testament (40–66)
  2. Apocrypha (67–81)

## Formatting of the Text

Because it is a plain text file, all formatting is plain.

  + Paragraphs are denoted via pilcrows (¶).  Bear in mind that the
    KJV's paragraphication abbruptly ends half-way through the book of
    Acts and is non-existent in the Apocrypha.  No one knows why the KJV
    translators decided (intentionally or unintentionally).  We do,
    however, know that they hated the Apocrypha and did not want to
    translate it initially (which is why it got the short-end of the
    stick in general), but it is just unknown why they stopped using
    paragraph-divisions half-way through Acts.
    - Paragraphs I have added are denoted via `<<¶>>` in the Apocrypha
      and via `<<Κ.>>` in the New Testament.  I do plan on adding some
      to the Old Testament, using `<<פ>>`.  Removing these is fairly
      easy.
  + *Italicised text* is denoted by round-brackets \[()\], or
    "parentheses".
  + ***Red letters*** are denoted by UTF-8 left and right English
    quotation marks (“”)[^9].  Each line of a quote is denoted by
    `<jesusQUOTE></jesusQUOTE>` HTML tags.
  + Bracketed text is denoted via square-brackets (\[\]), or "brackets".

- - - -

**Fixing Italics**

If you would like to convert the round-brackets to italicised text, you
should do so using a find-and-replace program (like `sed` on \*NIX
systems.)  While the next section will discuss in detail a few ways to
actually interact with the TSV (using `awk`), without explaining what is
happening, here is an example that will create some markdown text:

``` sh
#!/bin/env bash
{
    # Create book header
    echo "# Genesis"
    echo ""
    # Create chapter header
    echo "## Chapter 1"
    echo ""
    # Get verses and make verses superscript
    awk                \
        -F "\t"        \
        '$1=="Genesis" && $4==1 { print "<sup>" $5 "<\/sup>" $6 }'\
        kjv_pcea.tsv | \
    # Convert italics
    sed "s/[()]/*/g"
} > gen-01.md
```

Output:

``` markdown
# Genesis

## Chapter 1

<sup>1</sup>In the beginning God created the heaven and the earth.
<sup>2</sup>And the earth was without form, and void; and darkness *was* upon the face of the deep. And the Spirit of God moved upon the face of the waters.
<sup>3</sup>And God said, Let there be light: and there was light.
[...]
<sup>31</sup>And God saw every thing that he had made, and, behold, *it was* very good. And the evening and the morning were the sixth day.
```

- - - -

**Fixing Red Letters**

If you would like to convert the quotation marks to red-letter text,
this will be a little more difficult.  You may wish to delete the
quotation marks, but bear in mind that you should replace them with bold
text—this is for colourblind individuals.  Nevertheless, you will want
to also be mindful of the `<jesusQUOTE></jesusQUOTE>` tags that enshroud
each line of a quote.

These can easily be replaced with **bold**
delimiters, such as `**<text>**` for Markdown.

For HTML, you could either keep the tags as they are and define them as
a style or replace them with `<div style="color:red;"></div>`.

Here is an example L<sup>A</sup>T<sub>E</sub>X output:

``` sh
#!/bin/env bash
{
    printf "%% \!TEX pdflatex\n"
    printf "%sdocumentclass{article}\n" "\\"
    echo ""
    printf "%susepackage[british]{babel}\n" "\\"
    echo ""
    printf "%susepackage{xcolor}\n" "\\"
    echo ""
    # Create book header
    printf "%stitle{The Gospel after John{%stextquotesingle}s Recollection}\n" "\\" "\\"
    printf "%sauthor{%s}\n" "\\" \
        "$( getent passwd "$USERNAME" | awk -F ":" '{ print $5 }' )"
    printf "%sdate{%stoday}\n" "\\" "\\"
    echo ""
    printf "%sbegin{document}\n" "\\"
    echo ""
    printf "%smaketitle\n" "\\"
    echo ""
    # Create chapter header
    printf "%ssection*{Chapter 4}\n" "\\"
    echo ""
    # Get verses and make verses superscript
    awk                \
        -F "\t"        \
        '$1=="John" && $4==4 { print "\\textsuperscript\{" $5 "\}" $6 " \\\\" }'\
        kjv_pcea.tsv | \
    # Convert italics
    sed 's/(/\\textit{/g;s/)/}/g' | \
    # Convert red letters
    sed 's/<jesusQUOTE>/\\textcolor{red}{/g;s/<\/jesusQUOTE>/}/g'
    printf "%send{document}" "\\"
} > john-04.tex
```

The output will look something like this:

``` tex
% !TEX pdflatex
\documentclass{article}

\usepackage[british]{babel}

\usepackage{xcolor}

\title{The Gospel after John{\textquotesingle}s Recollection}
\author{Matthew Tate Scarbrough}
\date{\today}

\begin{document}

\maketitle

\section*{Chapter 4}

\textsuperscript{1}When therefore the Lord knew how the Pharisees had heard that Jesus made and baptized more disciples than John, \\
[...]
\textsuperscript{6}Now Jacob's well was there. Jesus therefore, being wearied with \textit{his} journey, sat thus on the well: \textit{and} it was about the sixth hour. \\
\textsuperscript{7}There cometh a woman of Samaria to draw water: Jesus saith unto her, “\textcolor{red}{Give me to drink.}” \\
[...]
\textsuperscript{12}Art thou greater than our father Jacob, which gave us the well, and drank thereof himself, and his children, and his cattle? \\
\textsuperscript{13}Jesus answered and said unto her, “\textcolor{red}{Whosoever drinketh of this water shall thirst again:} \\
\textsuperscript{14}\textcolor{red}{But whosoever drinketh of the water that I shall give him shall never thirst; but the water that I shall give him shall be in him a well of water springing up into everlasting life.}” \\
[...]
\textsuperscript{47}When he heard that Jesus was come out of Judaea into Galilee, he went unto him, and besought him that he would come down, and heal his son: for he was at the point of death. \\
[...]
\textsuperscript{50}Jesus saith unto him, “\textcolor{red}{Go thy way; thy son liveth.}” And the man believed the word that Jesus had spoken unto him, and he went his way. \\
\textsuperscript{51}And as he was now going down, his servants met him, and told \textit{him}, saying, Thy son liveth. \\
\textsuperscript{52}Then inquired he of them the hour when he began to amend. And they said unto him, Yesterday at the seventh hour the fever left him. \\
\textsuperscript{53}So the father knew that \textit{it was} at the same hour, in the which Jesus said unto him, “\textcolor{red}{Thy son liveth:}” and himself believed, and his whole house. \\
\textsuperscript{54}This \textit{is} again the second miracle \textit{that} Jesus did, when he was come out of Judaea into Galilee. \\
```

This may be hard to visualize, so if you would like to see the actual
PDF, go hither:

  + [john-04.pdf](https://drive.google.com/file/d/1jGiQRjzmjArD5w55hr1kfBSoOd4Ns226/view?usp=sharing)

## How to interface with the TSV

As previously stated, the TSV is intended for use with Bontibon's `kjv`
terminal program.  However, you can actually just interface with it
directly anyway you would choose.  The recommended way, however, is via
`awk`[^10]—a commandline utility that allows easy parsing of text
separated by delimiters (such as tabs).  By default, `awk` assumes that
single spaces ( ) delimit the various values of a file.  This means that
to work with this *T*SV, you have to use the flag `-F` and tell it to
use tabs.  This means that all the following commands will be prefixed
with:

``` sh
awk -F "\t"
```

**Listing Books**

As you may cleverly remember (as it is mentioned above), the first
column of each line contains the full book name.  Logically, then, you
need only tell AWK to query the first line.  However, you may not
anticipate that if you only query the first column:

``` sh
awk -F "\t" '{ print $1 }' kjv_pcea.tsv
```

You should get something in the order of 36,643 (36.643) lines of
duplicate book names.  On a \*NIX system, you can easily pipe the output
into the `uniq[ue]` program to delete all duplicate lines.  If you want
to alphabetically sort them, then you can pipe `uniq[ue]`'s output into
`sort`:

``` sh
# Traditional order
awk -F "\t" '{ print $1 }' kjv_pcea.tsv | uniq

# Alphabetical order
awk -F "\t" '{ print $1 }' kjv_pcea.tsv | uniq | sort
```

**Printing Chapters and Verses**

Following in with this line of logic, you can easily print chapters and
verses by adding `$4`, `$5`, and `$6`.  However, simply just executing
`{ print $4 $5 $6 }` would print all the text and numbers squished
together.  To make it human-readable, you should add some delimiters
between the columns, such as a colon between the numbers and a space
between the verse number and text:

``` sh
awk -F "\t" '{ print $4 ":" $5 " " $6 }' kjv_pcea.tsv
```

Of course, you still have the 37K-line output issue.

- - - -

If you want to narrow the search down to just the book of Genesis,
typing in the following will print all lines in Genesis:

``` sh
awk -F "\t" '$1 == "Genesis"' kjv_pcea.tsv
```

and the output:

    Genesis\tGe\t1\t1\t1\tIn the beginning God created the heaven and the earth.
    [...]
    Genesis\tGe\t1\t50\t26\tSo Joseph died, (being) an hundred and ten years old: and they embalmed him, and he was put in a coffin in Egypt.

- - - -

If we combine the previous example with the colon and the space between
columns `$4`–`$6`:

``` sh
awk -F "\t" '$1 == "Genesis" { print $4 ":" $5 " " $6 }' kjv_pcea.tsv
```

you will get:

    1:1 In the beginning God created the heaven and the earth.
    [...]
    50:26 So Joseph died, (being) an hundred and ten years old: and they embalmed him, and he was put in a coffin in Egypt.

- - - -

This can be further narrowed down by telling it where both
`$1=="Genesis"` AND also `$4` (the chapter) is equal to a number.  So if
we want to print only Genesis 1, well:

``` sh
awk -F "\t" '$1 == "Genesis" && $4 == "1" { print $4 ":" $5 " " $6 }' kjv_pcea.tsv
```

    1:1 In the beginning God created the heaven and the earth.
    [...]
    1:31 And God saw every thing that he had made, and, behold, (it was) very good. And the evening and the morning were the sixth day.

- - - -

Of course, we may only want verses 3–5 because they are rather
interesting, well you need "Genesis" AND chapter "1" AND from rows 3–5.
_NR_ stands for _number of record_ (records on a tab or rows):

``` sh
awk -F "\t" '$1=="Genesis" && $4==1 && NR==3, NR==5 { print $4 ":" $5 " " $6 }' kjv_pcea.tsv
```

Output:

    1:3 And God said, Let there be light: and there was light.
    1:4 And God saw the light, that (it was) good: and God divided the light from the darkness.
    1:5 And God called the light Day, and the darkness he called Night. And the evening and the morning were the first day.

## Disclaimer

This project is unaffiliated with and does not explicitly endorse the
views of Bibleprotector.com, the Australia-based ministry which produced
the official PCE.  The version of the PCE presented here should not be
considered "the PCE", even if it should be letter-for-letter,
blot-per-blot the same.

Also, though made to work with Bontibon's Bible Program, the
abbreviations for book names are completely different.  The idea is that
they do not match a partial match of the given book name.  Thus,
though the most common abbreviation for _Genesis_ is `Gen`, this is a
partial match of _Genesis_.  Thus, the abbreviation used in this TSV is
`Gn`.

## Footnotes

[^1]: The Pure-Cambridge Edition (PCE) Bible is a Critical edition of
the King James Version Bible, produced by an Australia-based ministry,
called the _Bible Protector_.
    
    The following links will take you to (1) the official PCE, (2) Bible
Protector's reasons behind using/creating the PCE, (3) the Bible
Protector mission-statement, (4) and the Bible Protector homepage:
    
    1. <http://bibleprotector.com/KJB-PCE-MINION.pdf>
    2. <http://bibleprotector.com/theprotector.htm>
    3. <http://bibleprotector.com/purpose.htm>
    4. <http://bibleprotector.com/index.htm>

[^2]: First published in 1611, the King James Version (KJV) Bible is the
most popular and beloved translation of the Bible into English.  In a
time where English Christians were tired of correcting their previous
translations and updating the out-dated language for a modern audience,
King James I's advisors recommended he pay for a new translation of the
Bible to ease Anglican England as they struggled to cope with a Scottish
Presbyterian taking the throne and becoming head of the Church, as well
as ease the tensions between the Anglicans and Puritans.
    
    Using the Bishop's Bible as a base, after years of study and prayer,
three teams of England's best scholars and linguists would quickly
translate a new version of the Bible and spend the next several years
arguing in Latin over how best to word an English Bible—it should be
simple for a farmer to understand, but elegant and poetic.  After years
of labour, the rough draft would be finished; the translators, pleased
with their work, would leave hope for the future, where better
manuscripts and better scholarship could improve upon their limited
resources.
    
    There are also two manuscript families of the KJV.  It essentially
amounts to Oxford opting to preserve typos and Cambridge electing to fix
them.
    
    The following links will take you to (1) a semi-interesting
documentary produced by the BBC, and (2) a video touching on some of the
controversy around the KJV:
    
    1. <https://www.youtube.com/watch?v=aa4f9c8lnog>
    2. <https://www.youtube.com/watch?v=_wvsJk8GL6M>

[^3]: Apocrypha is a catch-all-term for books not considered canonical.
Among non-Catholic/-Orthodox, Catholics, and Orthodox, contention is
over a group of Apocrypha, called _deuterocanonical_ (Second-Canon).
These are books that Catholics and Orthodox consider part of the Old
Testament, but Judaism and other Christians do not.
    
    The Catholic deuterocanon consists of just seven other books:
    
    + Tobit,
    + Judith,
    + Wisdom of Solomon,
    + Wisdom of Sirach,
    + Baruch,
    + I Maccabees,  
      and
    + II Maccabees.
    
    The Orthodox canon consists of (in parentheses are books missing
from this project because they were not translated by the King James
translators):
    
    + Tobit;
    + Judith;
    + Additions to Esther \[Should be in I or II Esdras\];
    + Wisdom of Solomon;
    + Wisdom of Sirach;
    + Baruch;
    + Epistle of Jeremiah;
    + additions to Daniel:
      - Song of the Three Children,
      - Susanna,
      - Bel and the Dragon;
    + Psalm 151 (Untranslated);
    + I Maccabees;
    + II Maccabees;
    + III Maccabees (Untranslated);
    + IV Maccabees (Untranslated);
    + I Esdras;  
      and
    + A portion of II Esdras
      - the Prayer of Manasseh.
    
    The following links will take you to (1) an online edition of the
Douay-Rheims Version (DRV) Bible and the Latin Vulgate, and (2) the
Orthodox Wiki:
    
    1. <http://drbo.org/>
    2. <https://orthodoxwiki.org/Apocrypha>

[^4]: _Red letters_ are acclaimed direct-quotations of Jesus in the New
Testament—so-called because they have been regularly type-set in red
text since Louis Klopsch first published his Red Letter Edition New
Testament in 1899.  Some newer editions use purple font.

[^5]: _Italicised text_ traditionally is helping-text added to
translations of the Bible to make them more poetic or to clarify grammar
or meaning, so-called because they are usually written in oblique or
italicised font.  In the original rough draft of the KJV, the basic
typeface was a _Byzantine/Gothic-style_ similar to Fraktur, but the
"italic text" was rendered in a normal, small Roman-style typeface
similar to Times New Roman.
    
    According to Samuel Ward, one of the KJV translators,
    
    > Words that it was anywhere necessary to insert into the text to
    > complete the meaning were to be distinguished by another
    > type\[face\], small roman...
    
    The following links will take you to (1) an excellent article on
typographical terminology; (2) an article on the italic text by Dr.
David L. Brown of the King James Bible Research Council; and two
Wikipedia pages—(3) one for the Fraktur font and (4) one for Times New
Roman:
    
    1. <https://www.canva.com/learn/typography-terms/>
    2. <https://kjbrc.org/the-use-of-italics-in-the-king-james-bible/>
    3. <https://en.wikipedia.org/wiki/Fraktur>
    4. <https://en.wikipedia.org/wiki/Times_New_Roman>

[^6]: _Bracketed-text_ is any content of the Bible by critical
scholarship of the Bible to not most likely not be part of the original
autographs.  It is so-named because it is usually denoted by being
placed in square-brackets (though some Bibles may use footnotes.)

[^7]: As of 03 January 2022, there is a file called `kjv.tsv` in the
`data/` directory.  You are meant to replace it with this file.
    
    + <https://github.com/bontibon/kjv>

[^8]: Luke Smith forked Bontibon's `kjv` terminal program to add the
Apocrypha.  The apocrypha in this project comes from his project.
    
    The following links are to (1) his personal website, (2) his Odysee
page, and (3) his fork of Bontibon's `kjv` program:
    
    1. <https://lukesmith.xyz/>
    2. <https://odysee.com/@Luke:7>
    3. <https://github.com/LukeSmithxyz/kjv>

[^9]: See U+201C and U+201D at <https://unicode-table.com/en/>

[^10]: AWK is a programming language for searching tables of data and
quickly parsing through it.  It was named for the initials of its three
creators: Alfred *A*ho, Peter *W*einberger, and Brian *K*ernighan.  For
an informal introduction to AWK, see this video:
    
    + <https://youtu.be/9YOZmI-zWok>
